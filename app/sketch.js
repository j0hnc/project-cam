var video;
var snaps = [];

function setup() {
	createCanvas(140 * 9, 110 * 3);
	background(0, 0, 0);
	video = createCapture(VIDEO);
	video.size(320, 240);
	video.position(width - 320, windowHeight - 250);
	button = createButton("Take a snap!");
	button.mousePressed(takeSnap);
	button.position(10, windowHeight - 70);
}

function takeSnap() {
	snaps.push(video.get());
}

function draw() {
	var w = 140, h = 110, x = 0, y = 0;
	for (var i = 0; i < snaps.length; i++) {
		image(snaps[i], x, y, w, h);
		x = x + w;
		if (x > width) {
			x = 0;
			y = y + h;
		}
	}
}