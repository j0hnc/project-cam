var video;
var snaps = [];
var counter = 0;
var total = 43;

function setup() {
	createCanvas(1280, 480);
	background(0, 0, 0);
	video = createCapture(VIDEO, ready);
	video.size(320, 240);
	video.hide();
}

var go = false;

function ready() {
	go = true;
}

function draw() {
	if (go) {
		snaps[counter] = video.get();
		counter++;
		if (counter == total) {
			counter = 0;
		}
	}
	var w = 80 * 2;
	var h = 60 * 2;
	var x = 0;
	var y = 0;
	for (var i = 0; i < snaps.length; i++) {
		var index = (i + frameCount) % snaps.length;
		image(snaps[index], x, y, w, h);
		x = x + w;
		if (x > width) {
			x = 0;
		    y = y + h;
		}
	}
}