var video, snaps = [];

function setup() {
	createCanvas(320 * 2, 240 * 2 - 50);
	background(157, 80, 187);
	video = createCapture(VIDEO);
	video.size(320 * 2, 240 * 2 - 10);
	video.hide();
	video.position(width - 320, windowHeight - 250);
}

function draw() {
	tint(200, 30, 100, 15);
	image(video, 0, 0);	
}

