var video;

function setup() {
	createCanvas(600, 460);
	background(0, 0, 0);
	video = createCapture(VIDEO);
	video.size(600, 500);
	video.hide();
}

function draw() {
	tint(random(255), random(255), random(255));
	image(video, mouseX, 0);
}